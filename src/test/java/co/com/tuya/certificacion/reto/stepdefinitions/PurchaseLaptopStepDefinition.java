package co.com.tuya.certificacion.reto.stepdefinitions;

import co.com.tuya.certificacion.reto.exceptions.FailedPurchaseException;
import co.com.tuya.certificacion.reto.questions.GetPurchase;
import co.com.tuya.certificacion.reto.tasks.AddLaptop;
import co.com.tuya.certificacion.reto.tasks.MakeThePurchase;
import co.com.tuya.certificacion.reto.userinterfaces.HomePage;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.Before;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;

import static co.com.tuya.certificacion.reto.utils.Constantes.CHROME;
import static co.com.tuya.certificacion.reto.utils.Constantes.PURCHASE_NOT_COMPLETED;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;


public class PurchaseLaptopStepDefinition {

    @Managed(driver = CHROME)
    WebDriver driver;

    @Before
    public void setTheStage() {
        OnStage.setTheStage(new OnlineCast());

    }

    @Dado("^que (.*) anadio al carrito una laptop$")
    public void queAnadioAlCarritoUnaLaptop(String nombre){
        OnStage.theActorCalled(nombre);
        theActorInTheSpotlight().can(BrowseTheWeb.with(driver));
        theActorInTheSpotlight().wasAbleTo(Open.browserOn(new HomePage()),
                AddLaptop.toCart());
    }

    @Cuando("^el llena los datos personales para la compra$")
    public void elLlenaLosDatosPersonalesParaLaCompra(DataTable dataPurchase) {
        theActorInTheSpotlight().attemptsTo(MakeThePurchase.withTheData(dataPurchase));
    }
    @Entonces("^visualizara en pantalla el mensaje (.*)$")
    public void visualizaraEnPantallaElMensaje(String message) {
        theActorInTheSpotlight().should(seeThat(GetPurchase.confirmationText(message)).
                orComplainWith(FailedPurchaseException.class, PURCHASE_NOT_COMPLETED));
    }
}
