package co.com.tuya.certificacion.reto.runners;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        glue = { "co.com.tuya.certificacion.reto.stepdefinitions"},
        features = "src/test/resources/features/purchase_laptop.feature",
        snippets = CucumberOptions.SnippetType.CAMELCASE)

public class PurchaseLaptop {
}
