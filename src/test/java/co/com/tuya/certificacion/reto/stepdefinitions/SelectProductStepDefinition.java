package co.com.tuya.certificacion.reto.stepdefinitions;

import co.com.tuya.certificacion.reto.exceptions.FailedInfoException;
import co.com.tuya.certificacion.reto.exceptions.FailedPurchaseException;
import co.com.tuya.certificacion.reto.questions.GetInfo;
import co.com.tuya.certificacion.reto.questions.GetPurchase;
import co.com.tuya.certificacion.reto.tasks.AddLaptop;
import co.com.tuya.certificacion.reto.tasks.SelectProduct;
import co.com.tuya.certificacion.reto.userinterfaces.HomePage;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;

import static co.com.tuya.certificacion.reto.utils.Constantes.*;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class SelectProductStepDefinition {

    @Managed(driver = CHROME)
    WebDriver driver;

    @Dado("^que (.*) ingreso al portal de DemoBlaze$")
    public void queIngresoAlPortalDeDemoBlaze(String nombre) {
        OnStage.theActorCalled(nombre);
        theActorInTheSpotlight().can(BrowseTheWeb.with(driver));
        theActorInTheSpotlight().wasAbleTo(Open.browserOn(new HomePage()));
    }

    @Cuando("^selecciona la laptop$")
    public void seleccionaLaLaptop() {
        theActorInTheSpotlight().attemptsTo(SelectProduct.toModel());
    }
    @Entonces("se visualizara en pantalla los siguientes campos")
    public void seVisualizaraEnPantallaLosSiguientesCampos(DataTable dataTable) {
        theActorInTheSpotlight().should(seeThat(GetInfo.confirmation(dataTable)).
                orComplainWith(FailedInfoException.class, INCORRECT_INFORMATION));
    }
}
