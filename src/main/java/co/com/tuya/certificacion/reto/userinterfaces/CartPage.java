package co.com.tuya.certificacion.reto.userinterfaces;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class CartPage extends PageObject {
    public static final Target BUTTON_PLACE_ORDER = Target.the("Presionar botón Place Order")
            .located(By.xpath("//button[normalize-space()='Place Order']"));
    public static final Target TEXT_NAME = Target.the("Ingresar nombre").located(By.id("name"));
    public static final Target TEXT_COUNTRY = Target.the("Ingresar País").located(By.id("country"));
    public static final Target TEXT_CITY = Target.the("Ingresar ciudad").located(By.id("city"));
    public static final Target TEXT_CREDITCARD = Target.the("Ingresar numero de tarjeta")
            .located(By.id("card"));
    public static final Target TEXT_MONTH = Target.the("Ingresar mes").located(By.id("month"));
    public static final Target TEXT_YEAR = Target.the("Ingresar año").located(By.id("year"));
    public static final Target BUTTON_PURCHASE = Target.the("Clic en botón Purchase")
            .located(By.xpath("//button[normalize-space()='Purchase']"));
}
