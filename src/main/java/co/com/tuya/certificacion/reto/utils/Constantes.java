package co.com.tuya.certificacion.reto.utils;

public class Constantes {
    public final static String CHROME = "chrome";
    public final static String URL = "https://www.demoblaze.com/index.html";
    public final static String PURCHASE_NOT_COMPLETED = "No se pudo completar la compra de la laptop";
    public final static String INCORRECT_INFORMATION = "LA información mostrada es incorrecta";
}
