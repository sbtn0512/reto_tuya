package co.com.tuya.certificacion.reto.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Hit;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.Keys;

import static co.com.tuya.certificacion.reto.userinterfaces.HomePage.*;
import static co.com.tuya.certificacion.reto.userinterfaces.ProductPage.BUTTON_ADD_TO_CART;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isCurrentlyVisible;

public class AddLaptop implements Task {
    public static AddLaptop toCart() {
        return instrumented(AddLaptop.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(SELECT_CATEGORY),
                Click.on(SELECT_MODEL),
                Click.on(BUTTON_ADD_TO_CART),
                WaitUntil.the(SELECT_CART, isCurrentlyVisible()).forNoMoreThan(3).seconds(),
                Hit.the(Keys.ENTER).into(SELECT_CART),
                Click.on(SELECT_CART));
    }
}
