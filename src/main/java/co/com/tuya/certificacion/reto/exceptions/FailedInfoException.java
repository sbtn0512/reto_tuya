package co.com.tuya.certificacion.reto.exceptions;

public class FailedInfoException extends AssertionError {

    public FailedInfoException(String message, Throwable cause) {
        super(message, cause);
    }
}
