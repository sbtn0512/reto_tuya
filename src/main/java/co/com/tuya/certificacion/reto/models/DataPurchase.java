package co.com.tuya.certificacion.reto.models;

public class DataPurchase {

    private String nombre;
    private String pais;
    private String ciudad;
    private String tarjetaCredito;
    private String mes;
    private String anio;

    public DataPurchase(String nombre, String pais, String ciudad, String tarjetaCredito, String mes, String anio) {
        this.nombre = nombre;
        this.pais = pais;
        this.ciudad = ciudad;
        this.tarjetaCredito = tarjetaCredito;
        this.mes = mes;
        this.anio = anio;
    }

    public String getNombre() {
        return nombre;
    }

    public String getPais() {
        return pais;
    }

    public String getCiudad() {
        return ciudad;
    }

    public String getTarjetaCredito() {
        return tarjetaCredito;
    }

    public String getMes() {
        return mes;
    }

    public String getAnio() {
        return anio;
    }
}
