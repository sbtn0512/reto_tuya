package co.com.tuya.certificacion.reto.userinterfaces;


import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class ProductPage extends PageObject {

    public static final Target BUTTON_ADD_TO_CART = Target.the("Click en el botón Add to cart")
            .located(By.xpath("//a[normalize-space()='Add to cart']"));
}
