package co.com.tuya.certificacion.reto.exceptions;

public class FailedPurchaseException extends AssertionError {

    public FailedPurchaseException(String message, Throwable cause) {
        super(message, cause);
    }
}
