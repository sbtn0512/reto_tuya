package co.com.tuya.certificacion.reto.questions;

import io.cucumber.datatable.DataTable;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import static co.com.tuya.certificacion.reto.userinterfaces.HomePage.*;
import static co.com.tuya.certificacion.reto.userinterfaces.ValidatePurchasePage.TEXT_PURCHASE;

public class GetInfo implements Question<Boolean> {

    private DataTable info;

    public GetInfo(DataTable info) {
        this.info = info;
    }

    public static GetInfo confirmation(DataTable info) {
        return new GetInfo(info);
    }
    @Override
    public Boolean answeredBy(Actor actor) {
        return (info.cell(1,0).equals(Text.of(NAME_MODEL).viewedBy(actor).asString()) &&
                (info.cell(1,1).equals(Text.of(PRICE_MODEL).viewedBy(actor).asString())) &&
                (info.cell(1,2).equals(Text.of(DESCRIPTION_MODEL).viewedBy(actor).asString())));
    }
}
