package co.com.tuya.certificacion.reto.userinterfaces;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class ValidatePurchasePage extends PageObject {

    public static final Target TEXT_PURCHASE = Target.the("Validar mensaje Thank you for your purchase!")
            .located(By.xpath("//h2[normalize-space()='Thank you for your purchase!']"));
    public static final Target PURCHASE_INFO = Target.the("Validar información compra ").located(
            By.xpath("//p[contains(@class,'lead text-muted')]"));
    public static final Target BUTTON_OK = Target.the("Presionar OK").located(
            By.xpath("//h2[normalize-space()='Thank you for your purchase!']")
    );
}
