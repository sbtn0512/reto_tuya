package co.com.tuya.certificacion.reto.userinterfaces;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;


import static co.com.tuya.certificacion.reto.utils.Constantes.URL;

@DefaultUrl(URL)
public class HomePage extends PageObject {
    public static final Target SELECT_CATEGORY = Target.the("Clic en la categoria Laptops ")
            .located(By.xpath("//a[normalize-space()='Laptops']"));
    public static final Target SELECT_MODEL = Target.the("Seleccionar el modelo laptop ")
            .located(By.xpath("//a[normalize-space()='MacBook air']"));
    public static final Target SELECT_CART = Target.the("Click en la opción Cart")
            .located(By.xpath("//a[normalize-space()='Cart']"));
    public static final Target NAME_MODEL = Target.the("Nombre del producto").
            located(By.className("name"));
    public static final Target PRICE_MODEL = Target.the("Precio del producto").
            located(By.className("price-container"));
    public static final Target DESCRIPTION_MODEL = Target.the("Descripción del producto").
            located(By.xpath("//div[@id= 'more-information']//p"));
}
