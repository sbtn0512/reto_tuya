package co.com.tuya.certificacion.reto.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import static co.com.tuya.certificacion.reto.userinterfaces.ValidatePurchasePage.TEXT_PURCHASE;

public class GetPurchase implements Question<Boolean> {

    private final String message;

    public GetPurchase(String message) {
        this.message = message;
    }

    public static GetPurchase confirmationText(String message) {
        return new GetPurchase(message);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        return message.equals(Text.of(TEXT_PURCHASE).viewedBy(actor).asString());
    }
}
