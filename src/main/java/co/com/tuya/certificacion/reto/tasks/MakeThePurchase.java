package co.com.tuya.certificacion.reto.tasks;

import io.cucumber.datatable.DataTable;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static co.com.tuya.certificacion.reto.userinterfaces.CartPage.*;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class MakeThePurchase implements Task {

    private DataTable dataPurchase;

    public MakeThePurchase(DataTable dataPurchase) {
        this.dataPurchase = dataPurchase;
    }

    public static MakeThePurchase withTheData(DataTable dataPurchase) {
        return instrumented(MakeThePurchase.class, dataPurchase);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(BUTTON_PLACE_ORDER),
                Enter.theValue(dataPurchase.cell(1,0)).into(TEXT_NAME),
                Enter.theValue(dataPurchase.cell(1,1)).into(TEXT_COUNTRY),
                Enter.theValue(dataPurchase.cell(1,2)).into(TEXT_CITY),
                Enter.theValue(dataPurchase.cell(1,3)).into(TEXT_CREDITCARD),
                Enter.theValue(dataPurchase.cell(1,4)).into(TEXT_MONTH),
                Enter.theValue(dataPurchase.cell(1,5)).into(TEXT_YEAR),
                Click.on(BUTTON_PURCHASE));
    }
}
