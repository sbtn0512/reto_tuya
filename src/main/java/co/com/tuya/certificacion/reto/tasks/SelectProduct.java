package co.com.tuya.certificacion.reto.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Hit;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.Keys;

import static co.com.tuya.certificacion.reto.userinterfaces.HomePage.*;
import static co.com.tuya.certificacion.reto.userinterfaces.ProductPage.BUTTON_ADD_TO_CART;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isCurrentlyVisible;

public class SelectProduct implements Task {

    public static SelectProduct toModel() {
        return instrumented(SelectProduct.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(SELECT_CATEGORY),
                Click.on(SELECT_MODEL));
    }
}