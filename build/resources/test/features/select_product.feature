#language: es

Característica: Yo como cliente
  Deseo filtrar los productos por laptops
  Para validar los campos que se muestran en el producto

  Escenario: Seleccionar una laptop para validar el nombre, descripción y precio
    Dado que Sebastian ingreso al portal de DemoBlaze
    Cuando selecciona la laptop
    Entonces se visualizara en pantalla los siguientes campos
      |Nombre      | Precio             | Descripcion                                                                                                                                                                            |
      |MacBook air | $700 *includes tax | 1.6GHz dual-core Intel Core i5 (Turbo Boost up to 2.7GHz) with 3MB shared L3 cache Configurable to 2.2GHz dual-core Intel Core i7 (Turbo Boost up to 3.2GHz) with 4MB shared L3 cache. |