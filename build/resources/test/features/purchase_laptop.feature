#language: es

 Característica: Yo como cliente
   Deseo realizar la compra de una laptop
   Para validar el funcionamiento del carrito de compras

  Escenario: Comprar una laptop en el portal de DemoBlaze
    Dado que Sebastian anadio al carrito una laptop
    Cuando el llena los datos personales para la compra
      | nombre    | pais     | ciudad   | tarjetaCredito| mes | anio  |
      | Sebastian | Colombia | Medellin | 123456         | 09  | 2022 |
    Entonces visualizara en pantalla el mensaje Thank you for your purchase!